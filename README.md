# Mobile programming

Development of a mobile application with Xamarin: OneClickSchool, an electronic register for schools students and their families.

Check out the [Documentation](documentation/documentation.pdf) for more.